/*Piece.java*/

import java.awt.*;

/**
 *  Represents a Normal Piece in Checkers61bl
 * @author 
 */

public class Piece {
  
    /**
    *  Define any variables associated with a Piece object here.  These
    *  variables MUST be private or package private.
    */
    private int mySide;
    private Board myBoard;
    private boolean myIsKing;
    protected String myImg;
    private boolean hasCaptured;

    /**
     * Initializes a Piece
     * @param  side The side of the Piece
     * @param  b    The Board the Piece is on
     */
    Piece(int side, Board b) {
        mySide = side;
        myBoard = b;
        myIsKing = false;
        if (mySide == 0) {
            myImg = "/img/pawn-fire.png";
        } else {
            myImg = "/img/pawn-water.png";
        }
    }

    /**
    * Returns the side that the piece is on
    * @return 0 if the piece is fire and 1 if the piece is water
    */
    public int side() {
        return mySide;
    }

    public boolean isKing() {
        return myIsKing;
    }

    public void setKing() {
        myIsKing = true;
        if (this.side() == 0) {
            myImg = "/img/pawn-fire-crowned.png";
        }
        else {
            myImg = "/img/pawn-water-crowned.png";
        }
    }

    public void setSide(int side) {
        mySide = side;
    }

    /**
    * Destroys the piece at x, y. ShieldPieces do not blow up
    * @param x The x position of Piece to destroy
    * @param y The y position of Piece to destroy
    */
    void getBlownUp(int x, int y) {
        if (!myBoard.notInBoard(x,y) && myBoard.pieceAt(x, y) != null && myBoard.pieceAt(x, y).getClass() != ShieldPiece.class) {
            myBoard.remove(x, y);
        }
    }

    /**
    * Does nothing. For bombs, destroys pieces adjacent to it
    * @param x The x position of the Piece that will explode
    * @param y The y position of the Piece that will explode
    */
    void explode(int x, int y) {
        return;
    }

    /**
    * Signals that this Piece has begun to capture (as in it captured a Piece)
    */
    void startCapturing() {
        this.hasCaptured = true;
    }

    /**
    * Returns whether or not this piece has captured this turn
    * @return true if the Piece has captured
    */
    public boolean hasCaptured() {
        return this.hasCaptured;
    }

    /**
    * Resets the Piece for future turns
    */
    public void finishCapturing() {
        this.hasCaptured = false;
    }

    public String image() {
        return myImg;
    }
}