import junit.framework.TestCase;

/**
 * Created by EunjinCho on 2016. 3. 10..
 */
public class BoardTest extends TestCase {

    public void testRemove() throws Exception {
        Board myBoard = new Board(false);
        myBoard.remove(6, 2);
        assertNull(myBoard.pieceAt(6, 2));
    }

    public void testCanSelect() throws Exception {
        Board myBoard = new Board(false);
        Piece piece = myBoard.remove(3,5);
        myBoard.place(piece,3,3);
        myBoard.select(2, 2);
        assertTrue(myBoard.canSelect(4,4));
    }

    public void testSelect() throws Exception {
        Board myBoard = new Board(false);
        myBoard.select(2,2);
        myBoard.move(2, 2, 3, 3);
        myBoard.endTurn();
        assertTrue(myBoard.canSelect(5,5));
    }

    public void testMove() throws Exception {
        Board myBoard = new Board(false);
        myBoard.select(2, 2);
        myBoard.move(2, 2, 3, 3);
        myBoard.endTurn();
        myBoard.move(5, 5, 4, 4);
        myBoard.endTurn();
        myBoard.move(3, 3, 5, 5);
    }

    public void testMultiCapture() throws Exception {
        Board myBoard = new Board(false);
        // 0
        myBoard.select(2, 2);
        myBoard.move(2, 2, 3, 3);
        myBoard.endTurn();
        // 1
        myBoard.select(5, 5);
        myBoard.move(5, 5, 4, 4);
        myBoard.endTurn();
        // 0
        myBoard.select(0, 2);
        myBoard.move(0, 2, 1, 3);
        myBoard.endTurn();
        // 1
        myBoard.select(7, 5);
        myBoard.move(7, 5, 6, 4);
        myBoard.endTurn();
        // 0
        myBoard.select(1, 1);
        myBoard.move(1, 1, 0, 2);
        myBoard.endTurn();
        // 1
        myBoard.select(1, 5);
        myBoard.move(1, 5, 0, 4);
        myBoard.endTurn();
        // 0
        myBoard.select(0, 0);
        myBoard.move(0, 0, 1, 1);
        myBoard.endTurn();
        // multi-capture takes place
        myBoard.select(4, 4);
        myBoard.move(4, 4, 2, 2); // should be okay
        assertTrue(myBoard.canSelect(0, 0));
    }
}