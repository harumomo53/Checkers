/*BombPiece.java*/

/**
 *  Represents a BombPiece ins Checkers61bl
 * @author 
 */

public class BombPiece extends Piece {
 
    /**
    *  Define any variables associated with a BombPiece object here.  These
    *  variables MUST be private or package private.
    */

    /**
    * Constructs a new BombPiece
    * @param  side what side this BombPiece is on
    * @param  b    Board that this BombPiece belongs to
    */
    public BombPiece(int side, Board b) {
        //YOUR CODE HERE
        super(side, b);
        if (side == 0) {
            myImg = "/img/bomb-fire.png";
        } else {
            myImg = "/img/bomb-water.png";
        }
    }

    @Override
    public void setKing() {
        super.setKing();
        if (this.side() == 0)
            myImg = "/img/bomb-fire-crowned.png";
        else
            myImg = "/img/bomb-water-crowned.png";
    }

    @Override
    void explode(int x, int y) {
        getBlownUp(x, y);
        getBlownUp(x-1, y-1);
        getBlownUp(x+1, y-1);
        getBlownUp(x, y-1);
        getBlownUp(x+1, y);
        getBlownUp(x+1, y+1);
        getBlownUp(x, y+1);
        getBlownUp(x-1, y+1);
        getBlownUp(x-1, y);
    }

}
