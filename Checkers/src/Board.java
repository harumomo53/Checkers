/*Board.java*/

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

/**
 * Represents a Board configuration of a game of Checkers61bl
 * @author
 */
public class Board {

    /**
    *  Define any variables associated with a Board object here.  These
    *  variables MUST be private.
    */
    private Piece[][] myBoard;
    private int numFirePieces = 12;
    private int numWaterPieces = 12;
    protected int curTurn = 0; // fire starts first
    private Point selectedPieceSpot;
    private boolean hasMoved;
    private boolean canEndTurn;


    /**
    * Constructs a new Board
    * @param  shouldBeEmpty if true, add no pieces
    */
    public Board(boolean shouldBeEmpty) {
        myBoard = new Piece[8][8];
        selectedPieceSpot = null;
        hasMoved = false;
        canEndTurn = false;

        if (shouldBeEmpty) {
            return;
        } else {
            for (int i = 0; i < myBoard.length; i++) {
                for (int j = 0; j < myBoard[0].length; j++) {
                    // Fire Normal Piece
                    if (j == 0 && i % 2 == 0) {
                        myBoard[i][j] = new Piece(0, this);
                    }
                    // Fire Shield Piece
                    else if (j == 1 && i % 2 == 1) {
                        myBoard[i][j] = new ShieldPiece(0, this);
                    }
                    // Fire Bomb Piece
                    else if (j == 2 && i % 2 == 0) {
                        myBoard[i][j] = new BombPiece(0, this);
                    } else if (j == 3 || j == 4) {
                        continue;
                    }
                    // Water Bomb Piece
                    else if (j == 5 && i % 2 == 1) {
                        myBoard[i][j] = new BombPiece(1, this);
                    }
                    // Water Shield Piece
                    else if (j == 6 && i % 2 == 0) {
                        myBoard[i][j] = new ShieldPiece(1, this);
                    }
                    // Water Normal Piece
                    else if (j == 7 && i % 2 == 1) {
                        myBoard[i][j] = new Piece(1, this);
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public boolean notInBoard(int x, int y)
    {
        return (x > 7 || x < 0 || y > 7 || y < 0);
    }


    /**
    * gets the Piece at coordinates (x, y)
    * @param  x X-coordinate of Piece to get
    * @param  y Y-coordinate of Piece to get
    * @return   the Piece at (x, y)
    */
    public Piece pieceAt(int x, int y) {
        if (myBoard[x][y] == null || notInBoard(x, y)) {
            return null;
        } else {
            return myBoard[x][y];
        }
    }

    /**
    * Places a Piece at coordinate (x, y)
    * @param p Piece to place
    * @param x X coordinate of Piece to place
    * @param y Y coordinate of Piece to place
    */
    public void place(Piece p, int x, int y) {
        if (p == null || notInBoard(x, y)) {
            return;
        } else {
            myBoard[x][y] = p;
        }
    }

    /**
    * Removes a Piece at coordinate (x, y)
    * @param  x X coordinate of Piece to remove
    * @param  y Y coordinate of Piece to remove
    * @return   Piece that was removed
    */
    public Piece remove(int x, int y) {
        Piece toRtn = this.pieceAt(x, y);
        myBoard[x][y] = null;
        if (toRtn.side() == 0) {
            numFirePieces--;
        } else {
            numWaterPieces--;
        }
        return toRtn;
    }

    /**
    * Determines if a Piece can be selected
    * @param  x X coordinate of Piece
    * @param  y Y coordinate of Piece to select
    * @return   true if the Piece can be selected
    */
    public boolean canSelect(int x, int y) {

        // A square with a piece selected
        if (this.pieceAt(x, y) != null) {
            if (this.pieceAt(x, y).side() != curTurn) {
                return false;
            }
            // the player hasn't selected a piece or
            if (selectedPieceSpot == null) {
                return true;
            }
            // the player has selected a piece but didn't move
            if (selectedPieceSpot!= null && !hasMoved) {
                return true;
            }
        }
        // An empty square selected
        else if (this.pieceAt(x, y) == null) {
            if (selectedPieceSpot != null) {
                // the player has selected a piece which hasn't moved yet
                // and is selecting an empty spot which is a valid move for the previously selected piece
                if (!hasMoved) {
                    ArrayList<Point> canMoveToArr = canMoveFrom(selectedPieceSpot.x, selectedPieceSpot.y);
                    for (Point p: canMoveToArr) {
                        if (p.x == x && p.y == y) {
                            return true;
                        }
                    }
                    return false;
                }
                // the player has selected a piece, captured, and has selected another valid capture destination
                // multi-capture situation

                if (hasMoved && this.pieceAt(selectedPieceSpot.x, selectedPieceSpot.y).hasCaptured()) {
                    ArrayList<Point> canMoveToArr = canMoveFrom(selectedPieceSpot.x, selectedPieceSpot.y);
                    for (Point p: canMoveToArr) {
                        if (p.x == x && p.y == y) {
                            return true;
                        }
                    }
                }

            }
        }
        return false;
    }


    /**
     * ArrayList of valid spots to go from fromX, fromY
     * @return
     */
    public ArrayList<Point> canMoveFrom(int fromX, int fromY) {
        ArrayList<Point> canMoveToArr = new ArrayList<>();
        // if is king, we move in 4 directions
        if (this.pieceAt(fromX, fromY).isKing()) {
            if (!notInBoard(fromX - 1, fromY - 1) && this.pieceAt(fromX - 1, fromY - 1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                canMoveToArr.add(new Point(fromX - 1, fromY - 1));
            }

            if (!notInBoard(fromX - 2, fromY - 2) && this.pieceAt(fromX - 2, fromY - 2) == null && pieceAt(fromX - 1, fromY - 1) != null &&
                    (this.pieceAt(fromX - 1, fromY - 1).side() == 1 && this.pieceAt(fromX, fromY).side() == 0 ||
                            this.pieceAt(fromX - 1, fromY - 1).side() == 0 && this.pieceAt(fromX, fromY).side() == 1)) {
                canMoveToArr.add(new Point(fromX - 2, fromY - 2));
            }

            if (!notInBoard(fromX + 1, fromY - 1) && this.pieceAt(fromX + 1, fromY - 1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                canMoveToArr.add(new Point(fromX + 1, fromY - 1));
            }

            if (!notInBoard(fromX + 2, fromY - 2) && this.pieceAt(fromX + 2, fromY - 2) == null && pieceAt(fromX + 1, fromY - 1) != null &&
                    (this.pieceAt(fromX + 1, fromY - 1).side() == 1 && this.pieceAt(fromX, fromY).side() == 0 ||
                            this.pieceAt(fromX + 1, fromY - 1).side() == 0 && this.pieceAt(fromX, fromY).side() == 1)) {
                canMoveToArr.add(new Point(fromX + 2, fromY - 2));
            }

            if (!notInBoard(fromX + 1, fromY + 1) && this.pieceAt(fromX + 1, fromY + 1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                canMoveToArr.add(new Point(fromX + 1, fromY + 1));
            }

            if (!notInBoard(fromX + 2, fromY + 2) && this.pieceAt(fromX + 2, fromY + 2) == null && pieceAt(fromX + 1, fromY + 1) != null &&
                    (this.pieceAt(fromX + 1, fromY + 1).side() == 1 && this.pieceAt(fromX, fromY).side() == 0 ||
                            this.pieceAt(fromX + 1, fromY + 1).side() == 0 && this.pieceAt(fromX, fromY).side() == 1)) {
                canMoveToArr.add(new Point(fromX + 2, fromY + 2));
            }

            if (!notInBoard(fromX - 1, fromY + 1) && this.pieceAt(fromX - 1, fromY + 1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                canMoveToArr.add(new Point(fromX - 1, fromY + 1));
            }

            if (!notInBoard(fromX - 2, fromY + 2) && this.pieceAt(fromX - 2, fromY + 2) == null && pieceAt(fromX - 1, fromY + 1) != null &&
                    (this.pieceAt(fromX - 1, fromY + 1).side() == 1 && this.pieceAt(fromX, fromY).side() == 0 ||
                            this.pieceAt(fromX - 1, fromY + 1).side() == 0 && this.pieceAt(fromX, fromY).side() == 1)) {
                canMoveToArr.add(new Point(fromX - 2, fromY + 2));
            }

        } else {
            // if water side
            if (this.pieceAt(fromX, fromY).side() == 1) {
                // if bottom left is empty and has not captured
                if (!notInBoard(fromX-1, fromY-1) && this.pieceAt(fromX-1, fromY-1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                    canMoveToArr.add(new Point(fromX-1, fromY-1));
                }
                // if bottom left is opponent, then we can move to the deeper bottom left
                if (!notInBoard(fromX-2, fromY-2) && this.pieceAt(fromX-2, fromY-2) == null && pieceAt(fromX-1, fromY-1) != null && this.pieceAt(fromX-1, fromY-1).side() == 0) {
                    canMoveToArr.add(new Point(fromX-2, fromY-2));
                }
                // if bottom right is empty and has not captured
                if (!notInBoard(fromX+1, fromY-1) && this.pieceAt(fromX+1, fromY-1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                    canMoveToArr.add(new Point(fromX+1, fromY-1));
                }
                // if bottom right is opponent, then we can move to the deeper bottom left
                if (!notInBoard(fromX+2, fromY-2) && this.pieceAt(fromX+2, fromY-2) == null && pieceAt(fromX+1, fromY-1) != null && this.pieceAt(fromX+1, fromY-1).side() == 0) {
                    canMoveToArr.add(new Point(fromX+2, fromY-2));
                }
            }
            // if fire side
            else {
                // if top left is empty and has not captured
                if (!notInBoard(fromX-1, fromY+1) && this.pieceAt(fromX-1, fromY+1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                    canMoveToArr.add(new Point(fromX-1, fromY+1));
                }
                // if top left is opponent, then we can move to the higher top left
                if (!notInBoard(fromX-2, fromY+2) && this.pieceAt(fromX-2, fromY+2) == null && pieceAt(fromX-1, fromY+1) != null && this.pieceAt(fromX-1, fromY+1).side() == 1) {
                    canMoveToArr.add(new Point(fromX-2, fromY+2));
                }
                // if top right is empty and has not captured
                if (!notInBoard(fromX+1, fromY+1) && this.pieceAt(fromX+1, fromY+1) == null && !this.pieceAt(fromX, fromY).hasCaptured()) {
                    canMoveToArr.add(new Point(fromX+1, fromY+1));
                }
                // if top right is opponent, then we can move to the deeper bottom left
                if (!notInBoard(fromX+2, fromY+2) && this.pieceAt(fromX+2, fromY+2) == null && pieceAt(fromX+1, fromY+1) != null && this.pieceAt(fromX+1, fromY+1).side() == 1) {
                    canMoveToArr.add(new Point(fromX+2, fromY+2));
                }
            }
        }
        return canMoveToArr;
    }

    /**
    * Selects a square. If no Piece is active, selects the Piece and
    * makes it active. If a Piece is active, performs a move if an empty
    * place is selected. Else, allows you to reselect Pieces
    * @param x X coordinate of place to select
    * @param y Y coordinate of place to select
    */
    public void select(int x, int y) {
        if (canSelect(x, y)) {
            // if you select a square with a piece
            if (this.pieceAt(x, y) != null) {
                Point toPut = new Point(x, y);
                this.selectedPieceSpot = toPut;
            } else {
                move(this.selectedPieceSpot.x, this.selectedPieceSpot.y, x, y);
                hasMoved = true;
            }
            highlight(x, y, StdDrawPlus.WHITE);
        } else {
            return;
        }
    }

    private void highlight(int x, int y, Color color) {
        StdDrawPlus.setPenColor(color);
        StdDrawPlus.filledSquare(x + 0.5, y + 0.5, 0.5);
        if (this.selectedPieceSpot != null) {
            Piece selectedP = this.pieceAt(this.selectedPieceSpot.x, this.selectedPieceSpot.y);
            StdDrawPlus.picture(x + .5, y + .5, selectedP.image(), 1, 1);
        }
    }

    /**
    * Moves the piece located at Piece at (x1, y1) to (x2, y1)
    * @param x1 Original X coordinate of p
    * @param y1 Origin Y coordinate of p
    * @param x2 X coordinate to move to
    * @param y2 Y coordinate to move to
    */
    public void move(int x1, int y1, int x2, int y2) {

        myBoard[x2][y2] = myBoard[x1][y1];
        myBoard[x1][y1] = null;


        // if reaches at the end
        if (this.pieceAt(x2, y2).side() == 0 && y2 == 7)
            this.pieceAt(x2, y2).setKing();
        if (this.pieceAt(x2, y2).side() == 1 && y2 == 0)
            this.pieceAt(x2, y2).setKing();

        this.selectedPieceSpot = new Point(x2, y2);

        // Capturing if needed
        if (Math.abs(x1-x2) == 2) {
            this.pieceAt(x2, y2).startCapturing();
            remove((x1+x2)/2, (y1+y2)/2);
            if (this.pieceAt(x2, y2).getClass() == BombPiece.class && this.pieceAt(x2, y2).hasCaptured()) {
                this.pieceAt(x2, y2).explode(x2, y2);
            }
            if (this.pieceAt(x2, y2) != null) {
                this.pieceAt(x2, y2).finishCapturing();
            }
        }
        this.selectedPieceSpot = null;
        this.canEndTurn = true;
    }

    /**
    * Determines if the turn can end
    * @return true if the turn can end
    */
    public boolean canEndTurn() {
        return canEndTurn;
    }

    /**
     * Ends the current turn. Changes the player.
     */
    public void endTurn() {
        // Change turn
        if (curTurn == 0) {
            curTurn = 1;
        } else {
            curTurn = 0;
        }
        if (this.selectedPieceSpot != null) {

        }
        this.canEndTurn = false;
        this.selectedPieceSpot = null;
        hasMoved = false;
        winner();
    }

    /**
    * Returns the winner of the game
    * @return The winner of this game
    */
    public String winner() {
        if (numFirePieces == 0 && numWaterPieces == 0) {
            return "Tie";
        } else if (numWaterPieces == 0) {
            return "Fire Won";
        } else if (numFirePieces == 0) {
            return "Water Won";
        } else {
            return null;
        }
    }

    /**
     * Draw the board with each squares colored
     */
    public void drawBoard() {

        for (int i = 0; i < myBoard.length; i++) {
            for (int j = 0; j < myBoard[0].length; j++) {
                if ((i + j) % 2 == 0) {
                    StdDrawPlus.setPenColor(StdDrawPlus.GRAY);
                } else {
                    StdDrawPlus.setPenColor(StdDrawPlus.RED);
                }
                StdDrawPlus.filledSquare(i + .5, j + .5, .5);
                Piece p = myBoard[i][j];
                if (p != null) {
                    StdDrawPlus.picture(i + .5, j + .5, p.image(), 1, 1);
                }
            }
        }
        // draw title
        StdDrawPlus.setFont(new Font("serif", Font.BOLD, 20));
        StdDrawPlus.setPenColor(StdDrawPlus.BLUE);
        StdDrawPlus.text(4, 9, "Checkers by Sean");
    }


    /**
    * Starts a game
    */
    public static void main(String[] args) {
        // displays a blank 8x8 board
        StdDrawPlus.setCanvasSize(600, 600);
        StdDrawPlus.setXscale(0, 8);
        StdDrawPlus.setYscale(0, 8);
        Board b = new Board(false);
        while (true) {
            b.drawBoard();
            if (StdDrawPlus.mousePressed()) {
                System.out.println("side is : " + b.curTurn);
                double x = StdDrawPlus.mouseX();
                double y = StdDrawPlus.mouseY();
                System.out.println("X: " + (int)x);
                System.out.println("Y: " + (int)y);
                if (b.canSelect((int)x, (int)y)) {
                    System.out.println("CAN SELECT");
                    b.select((int)x, (int)y);
                }
            }
            //if (StdDrawPlus.isSpacePressed()) {
            //    System.out.println("spaceBarPressed");
            if (b.canEndTurn()) {
                b.endTurn();
                System.out.println("new side is: " + b.curTurn);
            }
            //}
            StdDrawPlus.show(20);
        }
    }
}