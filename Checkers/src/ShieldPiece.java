/*ShieldPiece.java*/

/**
 * Represents a ShieldPiece in Checkers61bl
 * @author 
 */

public class ShieldPiece extends Piece {

    /**
    *  Define any variables associated with a ShieldPiece object here.  These
    *  variables MUST be private or package private.
    */
  
    /**
    * Constructs a new ShieldPiece
    * @param  side what side this ShieldPiece is on
    * @param  b    Board that this ShieldPiece belongs to
    */
    public ShieldPiece(int side, Board b) {
        //YOUR CODE HERE
        super(side, b);
        if (side == 0) {
            myImg = "/img/shield-fire.png";
        } else {
            myImg = "/img/shield-water.png";
        }
    }

    @Override
    public void setKing() {
        super.setKing();
        if (this.side() == 0)
            myImg = "/img/shield-fire-crowned.png";
        else
            myImg = "/img/shield-water-crowned.png";
    }

}