Project1 - Checkers
===

This is my own GUI-supported version of the classic game of Checkers using ```StdDrawPlus``` library.

### Rules of Checkers 
This is quite different from the classic Checkers. 

1. The board is an 8x8 board, with water and fire pieces on the top and botton.
2. Each player starts with pieces in the three rows closest to them. The front row consists of **Bomb Pieces**, the second row consists of **Shield Pieces**, and the back consists of **Normal Pieces**. Only every other space on the Board is used.
3. The bottom left corner should have a black square and contain a normal fire piece.

Movements: <br \>
1. All pieces can only move and capture one square **diagonally**.
2. If a capture move is available, the player is not required to capture.
3. Fire team makes the first move. Fire pieces always move **upwards** and water pieces always move **downwards**.
4. Upon reaching the topmost row, fire pieces become "kinged" and are alllowed to move not only **diagonally forward**, but also **diagonally backwards**. The same can be said for water pieces that reach the bottommost row.
5. Capturing a piece works by **jumping over an opponent's piece**.
6. Multi-captures available

#### Normal Piece
1. Moves diagonally and captures diagonally. 
2. !Can multi-capture, meaning if performing a capture lands the piece in a position ready to perform another capture, **that piece may choose to do perform another capture in the same turn.**

#### Bomb Piece
1. !Performing a capture with a bomb piece causes an **explosion** in the destination landing. Explosions kill all pieces adjacent to the landing (pieces within a 3x3 block centered at the bomb's final position), as well as the exploding bomb. 
2. Howevever shield pieces will **not** be killed by a bomb explosion. 
3. Bombs do not discriminate; **all non-shield pieces** in the blast zone are killed. 
4. Chain reaction explosions do not occur.  
5. Bomb pieces **cannot** perform multi-captures, because they explode after the first capture.

#### Shield Piece
1. A normal checkers piece, excpet that it cannot be killed by bomb explosions. 
2. They can still be normally captured by any piece (including bombs).

#### King Piece
1. Any piece can be "kinged" upon reaching the furthest row from its origin side. King pieces can move and capture in four directions.
2. !A piece may capture, promote to a king, and capture again in the same turn if the orientation of the Board permits.

Changing turns after spacebar is pressed.

### The GUI
```StdDrawPlus.java``` has all the methods we need for creating a functional GUI.

* When a user clicks on a square:
    * First extract the coordinates of the piece
    * Check if you can select a piece (```canSelect``` in API)
    * If allowed to select, call select on its coordinates

* When a user presses spacebar:
    * Check if the current player is allowed to end their turn (```endTurn``` in API)
    * If so, make any changes necessary in ending the turn (e.g. switching the players)


### Required Methods/ API

#### ```Board.java```

* ```public static void main (String[] args)``` : starts a GUI supported version of the game
* ```public Board (boolean shouldBeEmpty)``` : If ```shouldBeEmpty``` is true, initializes an empty Board. Otherwise, initializes a Board with the default configuration.
* ```public Piece pieceAt(int x, int y)``` : Gets the piece at position (x,y) on the board, or null if there's no piece or (x,y) are out of bounds.
* ```public void place(Piece p, int x, int y)``` : Places p at (x,y). If (x,y) is out of bounds or p is null, does nothing. If another piece already exists at (x, y), p will replace that piece.
* ```public Piece remove(int x, int y)``` : Executes a remove. Returns the piece that was removed. If the input (x, y) is out of bounds or if there is no piece at (x, y), returns null 
* ```public boolean canSelect(int x, int y)``` : Returns true if the square (x, y) can be selected.
    * A square **with** a piece may be selected if it is the corresponding player’s turn and **one** of the following is true:
        * The player has not selected a piece yet.
        * The player has selected a piece, but did not move it.
    * An **empty** square may be selected if one of the following is true:
        *  The player **has selected** a Piece which **hasn’t moved yet** and is selecting an empty spot which is a valid move for the previously selected Piece.
        * The player has selected a Piece, captured, and has selected another valid capture destination. When performing multi-captures, you should only select the active piece once; all other selections should be valid destination points.
* ```public void select(int x, int y)``` : Selects the square at (x, y). This method assumes ```canSelect(x,y)``` returns true. In respect to the GUI, when you select a ```Piece```, color the background of the selected square white on the GUI via the pen color function.
    * For any piece to perform a capture, that piece must have been selected first. 
    * If you select a Piece, you are prepping that piece for movement. If you select an empty square (assuming canSelect returns true), you should move your most recently selected piece to that square.
* ```public void move(int x1, int y1, int x2, int y2)``` : Assumes Piece p's movement from (x1, y1) to (x2, y2) is valid. Moves the piece at (x1, y1), to (x2, y2) capturing any intermediate piece if applicable.
* ```public boolean canEndTurn()``` : Returns whether or not the the current player can end their turn. To be able to end a turn, a piece **must have moved** or **performed a capture**.
* ```public void endTurn()``` : Called at the end of each turn. Handles switching of players and anything else that should happen at the end of a turn.
* ```public String winner()``` : Returns the winner of the game: "Fire", "Water", "Tie", or null.
    * If only fire pieces remain on the board, fire wins. If only water pieces remain, water wins.
    * If no pieces remain (consider an explosion capture), no one wins and it is a tie.
    * If the game is still in progress (ie there are pieces from both sides still on the board) return null
    * Determine the winner solely by the number of pieces belonging to each team.

#### ```Piece.java```
* ```public Piece(int side, Board b)``` : Constructor for a normal Piece. 
    * side is the side of the Piece, either 0 for fire or 1 for water. 
    * b represents the Board that the piece is on. 
    * The new piece should not be a king.

* ```public int side()``` : Returns 0 if the piece is a fire piece, or 1 if the piece is a water piece.
* ```public boolean isKing()``` : Returns whether or not the piece has been crowned.
* ```public void startCapturing()``` : Called after a Piece has captured. Makes sure that the piece's hasCaptured() method returns true.
* ```public boolean hasCaptured()``` : Returns whether or not this Piece has captured another piece this turn.
* ```public void finishCapturing()``` : Called at the end of each turn on the Piece that moved. Makes sure the piece's hasCaptured() returns false.
* ```public void explode(int x, int y)``` : Causes this ```Piece``` to explode if it is a ```BombPiece```, else does nothing.
* ```public void getBlownUp(int x, int y)``` : Causes this Piece to blow up, removing it from the Board, unless it is a ShieldPiece.

#### ```ShieldPiece.java```
* ```public ShieldPiece(int side, Board b)``` 

#### ```BombPiece.java```
* ```public BombPiece(int side, Board b)```
* Keep in mind that BombPiece can only explode. We might mistakenly think that other kinds of Pieces like Normal or Shield piece could explode when captures Bomb piece, but it's not.

### BONUS
* Start a new game by pressing "n" in GUI mode (using ```stdDrawPlus.isNPressed()```)
* Implement an undo method, reverting the board to the previous state.
* Make some cool animations and sounds to accompany bomb explosions on the gui. StdDrawPlus currently doesn't support animations/sounds, so you should Google for another library that can help you out.
* Handle the stalemate situation. Have the game terminate however you like when the current player has pieces, but cannot move any of them.




